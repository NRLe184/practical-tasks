# -*- coding: utf-8 -*-
"""Задание 2.ipynb

Automatically generated by Colaboratory.

Original file is located at
    https://colab.research.google.com/drive/1byZQaXqH0wTarGMWuvsDByQS4geaNe2V

##Импорт библиотеки JSON
"""

import json

"""###Функция по считыванию JSON-файла"""

def read_json(json_file): #функция считывает json-файл
  with open(json_file, "r") as read_file:
    return json.load(read_file) #возвращает объект Python

"""###Функция сравнения двух JSON-объектов
Данная функция сравнивает два объекта на наличие изменений в структуре этих объектов(добавление/удаление) какого-либо элемента. На вход подаются два JSON-объекта, являющиеся частью другого JSON-объекта, который передаётся в переменной "json_object". "json_orig" – объект из оригинального файла JSON, "json_new" – объект из нового файла JSON. 

Значение добавленные/удалённых элементов не сравниваются, поскольку это не имеет смысла. Несмотря на то, что функция может отработать и без json_object, эта переменная передаётся, поскольку улучшает читаемость выводимых на экран данных

Алгоритм работы функции:

1. Происходит проверка типа данных. Если хотя бы один из поступивших объектов является списком, то сравнение не происходит, поскольку списки обрабатываются по другому алгоритму
2. Посредством преобразования обоих JSON-объектов к множеству и применению к ним метода difference выявляются добавленные/удалённые элементы. 
3. Если метод difference вернул пустое множество, то сообщение об удалении/добавлении не выводится
 
"""

def format_json_comprase(json_orig, json_new, json_object, json_prev): 
  if type(json_new) != type([]) and type(json_orig) != type([]):
    if set(json_new).difference(set(json_orig)) != set():
      if str(json_prev) != str(json_object): #случай, когда обе переменные содержат одинаековые имена возможен, если мы рассматриваем объекты на втором ярусе(втором уровне вложенности)
        print("К объекту " + str(json_object) + ", который является частью объекта " + str(json_prev) + " были добавлены следующие элементы:")
      else:
        print("К объекту " + str(json_object) + " были добавлены следующие элементы:")
      print(set(json_new).difference(set(json_orig)))
    if set(json_orig).difference(set(json_new)) != set():   
      if str(json_prev) != str(json_object):
        print("У объекта " + str(json_object) + ", который является частью объекта " + str(json_prev) + " были удалены следующие элементы:")
      else:
        print("У объекта " + str(json_object) + " были удалены следующие элементы:")
      print(set(json_orig).difference(json_new))

"""###Функция печати элементов, которыми различаются два списка
На вход функции подаётся список значений объектов, множество значений объектов и булевая переменная struct
Посредством перебора выводятся либо пары ключ/значение и множество добавленных/убранных объектов, либо только множество добавленных/убранных объектов(в зависимости от значения переменной struct)

```
print_changes(list_objects, set_objects, struct):
```


"""

def print_changes(list_objects, set_objects, struct):
  for i in list_objects:
        for j in set_objects:
          try:
            if struct == False:
              print(str(j) + " со значением " + str(i[j])) #вывод пар ключ:значение
            else:
              print(set_objects)
              return
          except:
            pass

"""###Основная функция сравнения двух JSON-файлов
Данная функция является рекурсивной и применяется для работы на втором и более глубоком ярусе(уровне вложенности)
На вход подаются:
Два JSON-объекта, являющиеся частью другого JSON-объекта, который передаётся в переменной "json_object". "json_orig" – объект из оригинального файла JSON, "json_new" – объект из нового файла JSON. В переменной struct(по умолчанию она равна False) передаётся булевое значение True\False, если значение False, то происходит сравнение JSON-файла по структуре и значениям, иначе - только по структуре, в переменной prev_obj содержится родительский объект объекта json_object, она нужна только для улучшения читаемости выводимых на экран данных 


```
  def deep_json_eq(json_orig, json_new, json_object, struct, prev_obj):
```


Алгоритм работы функции:

1. Сравнивается тип данных обоих элементов, а также если элементы данных двух объектов(старого и нового) не равны между собой и не являются списком либо объектом, то происходит вывод на экран старого и нового значения. Здесь же и только здесь применяется переменная "prev_object":


```
  if json_orig == json_new:
    pass
    #print("У объекта " + str(elem) + " изменений нет")
  else:
    try:
      if type(json_orig[json_object]) != type(json_new[json_object]):
        print("Изменение типа данных у " + str(json_object) + " с")
        print(type(json_orig[json_object]))
        print("на")
        print(type(json_new[json_object]))
      if json_orig[json_object] != json_new[json_object] and type(json_orig[json_object]) != type({}) and struct == False and type(json_orig[json_object]) != type([]):
        if type(json_orig) == type({}):
          print("Значение элемента " + str(json_object) + " объекта " + str(prev_obj) + " было изменено с " + str(json_orig[json_object]) + " на " + str(json_new[json_object]))        
        else:
          print("Значение элемента " + str(json_object) + " было изменено с " + str(json_orig[json_object]) + " на " + str(json_new[json_object]))      
    except:
      pass  
```


2. Поскольку переменная "prev_object" далее нигде не фигурирует(кроме вызова рекурсивной функции), то приравнием её к текущему значению переменной "json_object"


```
  prev_obj = json_object
```


3. По текущему типу данных смотрим, с чем работаем. Если тип данных переменной "json_orig" это dict(словарь), то мы работаем с объектом JSON, если же тип данных list(список), то мы работаем с массивом JSON

```
  if type(json_orig[json_object]) == type({})
  ...
  ...
  ...
  elif type(json_orig[json_object]) == type([]):
  ...
  ...
  ...
```



Если работаем с объектом JSON:

1. Сравниваем объекты "json_orig" и "json_new" между собой по структуре, используя ранее описанную функцию format_json_comprase



```
  format_json_comprase(json_orig[json_object], json_new[json_object], json_object)
```



2. Производим перебор элементов среди всех имеющихся с последующим вызовом для каждого элемента функции deep_json_eq(рекурсивная функция). Рекурсия будет продолжаться вплоть до момента, пока не будет произведено сравнение между элементами всех объектов



```
    for json_elem in json_orig[json_object]:
      deep_json_eq(json_orig[json_object], json_new[json_object], json_elem, struct, prev_obj) 
```



Если работаем с массивом JSON:

1. Объявляем пустые множества("json_orig_set" - для массива из оригинального файла и "json_orig_set" - для массива из нового файла) и пустые списки("list_orig" - для элементов массива из оригинального файла и "list_new" - для элементов массива из нового файла) и осуществляем перебор элементов обоих массивов(которые являются элементами данных объектов json_orig и json_new) с которыми на данный момент работаем. Каждое значение добавляется в множество(чтобы избежать дублирования одинаковых элементов в каждом из массивов, если таковые имеются), а в каждый список добавляются все перебираемые значения.
Если разница между двумя множествами присутствует, то происходит вывод этой разницы на экран посредством вспомогательной функции print_changes


```
  try:
    if json_orig[json_object] != json_new[json_object] and type(json_orig[json_object]) != type({}) and struct == False and type(json_orig[json_object]) != type([]):
      if type(json_orig) == type({}):
        if str(prev_obj) != str(json_object):
          print("Значение объекта " + str(json_object) + ", который является частью объекта " + str(prev_obj) + " было изменено с " + str(json_orig[json_object]) + " на " + str(json_new[json_object]))        
        else:
          print("Значение объекта " + str(json_object) + " было изменено с " + str(json_orig[json_object]) + " на " + str(json_new[json_object]))        
      else:
        if str(prev_obj) != str(json_object):
          print("Значение объекта " + str(json_object) + ", который является частью объекта " + str(prev_obj) + " было изменено с " + str(json_orig[json_object]) + " на " + str(json_new[json_object]))      
        else:
          print("Значение объекта " + str(json_object) + " было изменено с " + str(json_orig[json_object]) + " на " + str(json_new[json_object]))      
    if type(json_orig[json_object]) != type(json_new[json_object]):
      print("Изменение типа данных значения объекта " + str(json_object) + " с " + str(type(json_orig[json_object])) + " на " + str(type(json_new[json_object])))   
  except:
    pass  
```


2. Находим пересечение двух описанных выше множеств и если оно не является пустым, то переходим к следующему шагу

```
    list_objects_set = json_orig_set.intersection(json_new_set)
```
3. Объявляются три пустых списка "list_orig_json" хранит в себе элементы массива из оригинального файла, "list_new_json" - из нового, list_key_json хранит ключи, по которым можно обратиться к значениям каждого элемента массива(в качестве значения может может выступать объект, другое значение(например, число или строка) или также массив. 
Затем вызывается функция deep_json_eq, куда будут перереданы все значения списков list_orig_json, list_new_json, соответствующие им ключи и параметр struct и prev_obj
```
if list_objects_set != set():
      list_orig_json = []
      list_new_json = []
      list_key_json = []
      for elem in list_objects_set:
        list_key_json.append(elem)
      for elem_list in json_orig[json_object]:
        for elem in list_key_json:
          if elem in elem_list:
            list_orig_json.append(elem_list)
            break
      for elem_list in json_new[json_object]:
        for elem in list_key_json:
          if elem in elem_list:
            list_new_json.append(elem_list)
            break
      for i in range(len(list_orig_json)):           
        for j in range(len(list_key_json)):
          deep_json_eq(list_orig_json[i], list_new_json[i], list_key_json[j], struct, prev_obj)
```








"""

def deep_json_eq(json_orig, json_new, json_object, struct, prev_obj):
  try:
    #если значением является что угодно, но не объект или список
    #не критично, если уровень вложенности объектов друг в друга будет небольшим, но при достаточно глубокой вложенности результат работы этой части кода будет 
    #абсолютно нечитаемым 
    if json_orig[json_object] != json_new[json_object] and type(json_orig[json_object]) != type({}) and struct == False and type(json_orig[json_object]) != type([]):
      if type(json_orig) == type({}):
        if str(prev_obj) != str(json_object):
          print("Значение объекта " + str(json_object) + ", который является частью объекта " + str(prev_obj) + " было изменено с " + str(json_orig[json_object]) + " на " + str(json_new[json_object]))        
        else:
          print("Значение объекта " + str(json_object) + " было изменено с " + str(json_orig[json_object]) + " на " + str(json_new[json_object]))        
      else:
        if str(prev_obj) != str(json_object):
          print("Значение объекта " + str(json_object) + ", который является частью объекта " + str(prev_obj) + " было изменено с " + str(json_orig[json_object]) + " на " + str(json_new[json_object]))      
        else:
          print("Значение объекта " + str(json_object) + " было изменено с " + str(json_orig[json_object]) + " на " + str(json_new[json_object]))      
    if type(json_orig[json_object]) != type(json_new[json_object]):
      print("Изменение типа данных значения объекта " + str(json_object) + " с " + str(type(json_orig[json_object])) + " на " + str(type(json_new[json_object])))   
  except:
    pass  
  
  if type(json_orig[json_object]) == type({}): #если значением оказался объёкт
    print("\nОБЪЕКТ   {------------------------->" + str(json_object) + "<-------------------------}\n")
    format_json_comprase(json_orig[json_object], json_new[json_object], json_object, prev_obj) #функция по сравнению элементов, которые содержатся в объекте
    prev_obj = json_object #объект с предыдущей итерации, будет использоваться при выводе данных на экран для улучшения читаемости
    for json_elem in json_orig[json_object]:
      deep_json_eq(json_orig[json_object], json_new[json_object], json_elem, struct, prev_obj) #вызов рекурсивной функции для каждого объекта в рассматриваемом нами объекте
  
  elif type(json_orig[json_object]) == type([]): #если 
    json_orig_set = set()
    json_new_set = set()
    list_orig = []
    list_new = []
    #Сравниваем два списка по содержащимся в них значениям
    for elem_list in json_orig[json_object]:  
      json_orig_set = json_orig_set.union(elem_list) #множество значений списка объекта из оригинального файла
      list_orig.append(elem_list) 
    for elem_list in json_new[json_object]:
      json_new_set = json_new_set.union(elem_list) #множество значений списка объекта из нового файла
      list_new.append(elem_list)
    if json_new_set.difference(json_orig_set) != set(): #если 
      print("\nСПИСОК   [------------------------->" + str(json_object) + "<-------------------------]\n")
      if str(json_object) != str(prev_obj): #обе переменные будут хранить одинаковое значение имени объекта, только если он будет находиться на втором уровне вложенности
        print("К списку " + str(json_object) + ", который является частью объекта " + str(prev_obj) + " были добавлены следующие элементы:") 
      else:
        print("К списку " + str(json_object) + " были добавлены следующие элементы:") 
      print_changes(list_new, json_new_set.difference(json_orig_set), struct) #вызов функции печати результатов сравнения двух множеств
    if json_orig_set.difference(json_new_set) != set():   
      if str(json_object) != str(prev_obj):
        print("У списка " + str(json_object) + ", который является частью объекта " + str(prev_obj) + " были удалены следующие элементы:")
      else:
        print("У списка " + str(json_object) + " были удалены следующие элементы:")
      print_changes(list_orig, json_orig_set.difference(json_new_set), struct) #вызов функции печати результатов сравнения двух множеств
    list_objects_set = json_orig_set.intersection(json_new_set) #находим пересечения обоих множеств
    prev_obj = json_object #приравниваем значение предыдущего объекта к нынешнему
    if list_objects_set != set(): #если пересечением является непустое множество
      list_orig_json = []
      list_new_json = []
      list_key_json = []
      for elem in list_objects_set:
        list_key_json.append(elem) #ключи значений объектов рассматриваемого элемента выбранного массива 
      for elem_list in json_orig[json_object]:
        for elem in list_key_json:
          if elem in elem_list:
            list_orig_json.append(elem_list) #объекты выбранного массива оригинального файла
            break 
      for elem_list in json_new[json_object]:
        for elem in list_key_json:
          if elem in elem_list:
            list_new_json.append(elem_list) #объекты выбранного массива нового файла
            break
      for i in range(len(list_orig_json)):           
        for j in range(len(list_key_json)):
          deep_json_eq(list_orig_json[i], list_new_json[i], list_key_json[j], struct, prev_obj) #вызов функции самой себя; 
          #параметрами выступают объекты, являющиеся элементами списков, ключи к этим объектам, а также переменная struct и prev_obj

"""###Функция по сравнению JSON-файлов на первом ярусе(уровне вложенности)
Данная функция служит для обработки элементов, находящихся на первом уровне вложненности JSON-файла. На вход подаётся содержимое двух JSON-файлов json_orig, json_new и переменная struct, определяющая, в каком формате будет производиться сравнение: только по структуре или по структуре и значениям

Различает среди объектов те, значения которых также являются объектами/списками и вызывает для таких объектов рекурсивную функцию deep_json_eq

Алгоритм функции:

1. Два JSON-файла сравниваются между собой, если различий между ними нет, то программа завершает свою работу, иначе - переходим к шагу 2


```
  if(json_orig == json_new):
    print("Оба JSON-файла равны между собой")
    return
```


2. Объявляются два пустых множества, в которые добавляется элементы первого яруса соответствующих им JSON-файлов (json_orig_objects - старого JSON-файла, json_new_objects - для нового JSON-файла)


```
  json_orig_objects = set()
  json_new_objects = set()
  for elem in json_orig:
    json_orig_objects.add(elem)
  for elem in json_new:
    json_new_objects.add(elem)
```
3. Оба множества сравниваются между собой, если они равны, то происходит перебор значений первого уровня вложенности оригинального JSON-файла, если значением выступает объект или список, то для этой пары 

  ключ : значение == объект, то вызывается функция deep_json_eq(json_orig, json_new, elem)
  

```
  if json_orig_objects == json_new_objects:
    print("Изменений в структуре и значениях на первом ярусе нет \n")
    for elem in json_orig:
      if type(json_orig[elem]) == type({}) or type(json_orig[elem]) == type([]):
        deep_json_eq(json_orig, json_new, elem)
  else:
    if set(json_new_objects.difference(json_orig_objects)) != set():
      print("Были добавлены следующие элементы к первому ярусу:") 
      print(json_new_objects.difference(json_orig_objects))
    if set(json_orig_objects.difference(json_new_objects)) != set():
      print("Были удалены следующие элементы из первого яруса:")  
      print(json_orig_objects.difference(json_new_objects))
```


  Если же множества не равны между собой, то на экран выводится, какие элементы были добавлены/удалены, а также находится пересечение этих множеств

4. Сравниваем значения объектов(если значенеи struct == False), которые не являются списком или объектом из обоих множеств между собой(для повышения читаемости при выводе результатов сравнение значения объектов происходит в отдельном цикле, так как при вызове рекурсивной функции все результаты сравнения при выводе на экран перемешаются)


```
if struct == False:
      for elem in set_inter:
        if json_orig[elem] != json_new[elem] and type(json_orig[elem]) != type({}) and type(json_orig[elem]) != type([]):
          print("Значение объекта первого яруса " + str(elem) + " было изменено с " + str(json_orig[elem]) + " на " + str(json_new[elem]))     
```



5. Перебираем элементы найденного пересечения, которые являются ключами для значений json-объектов, выявляем изменения типов данных, а также проверяем каждое значение на тип данных. Если это объект или список, то вызываем функцию 
deep_json_eq(...), передавая ей соответствующие параметры         
 

```
    for elem in set_inter:
      if type(json_orig[elem]) != type(json_new[elem]):
        print("Изменение типа данных значения объекта " + str(elem) + " с")
        print(type(json_orig[elem]))
        print("на")
        print(type(json_new[elem]))
      if type(json_orig[elem]) == type({}) or type(json_orig[elem]) == type([]):
        deep_json_eq(json_orig, json_new, elem, struct, prev_obj = elem)
```


"""

def file_comprasion(json_orig, json_new, struct):
  if(json_orig == json_new):
    print("Оба JSON-файла равны между собой")
    return
  print("-------------------------Первый ярус-------------------------")
  json_orig_objects = set()
  json_new_objects = set()
  for elem in json_orig:
    json_orig_objects.add(elem) #добавляем ключи объектов оригинального файла первого яруса в множество
  for elem in json_new:
    json_new_objects.add(elem) #добавляем ключи объектов нового файла первого яруса в множество
  if json_orig_objects == json_new_objects:
    print("Изменений в структуре и значениях на первом ярусе нет \n")
    for elem in json_orig:
      if type(json_orig[elem]) == type({}) or type(json_orig[elem]) == type([]): #если рассматриваемое значение является элементом или списком, то вызываем функцию
        print("-----------------------------------Вложенные объекты-----------------------------------")
        deep_json_eq(json_orig, json_new, elem, struct, prev_obj = elem)
  else:
    if set(json_new_objects.difference(json_orig_objects)) != set():
      print("Были добавлены следующие элементы к первому ярусу:") 
      print(json_new_objects.difference(json_orig_objects))
    if set(json_orig_objects.difference(json_new_objects)) != set():
      print("Были удалены следующие элементы из первого яруса:")  
      print(json_orig_objects.difference(json_new_objects))
    set_inter = json_orig_objects.intersection(json_new_objects)
    if struct == False:
      for elem in set_inter:
        if json_orig[elem] != json_new[elem] and type(json_orig[elem]) != type({}) and type(json_orig[elem]) != type([]):
          print("Значение объекта первого яруса " + str(elem) + " было изменено с " + str(json_orig[elem]) + " на " + str(json_new[elem]))
        if type(json_orig[elem]) != type(json_new[elem]):
          print("Изменение типа данных значения объекта первого яруса" + str(elem) + " с " + str(type(json_orig[elem])) + " на " + str(type(json_new[elem])))     
    print("-----------------------------------Вложенные объекты-----------------------------------")
    for elem in set_inter:
      if type(json_orig[elem]) == type({}) or type(json_orig[elem]) == type([]): 
        deep_json_eq(json_orig, json_new, elem, struct, prev_obj = elem) #вызов функции для каждого объекта или списка

struct = None

struct = "struct"

print("Введите 'struct', чтобы осуществить сравнение только по структуре, иначе будет произведено сравнение как по структуре, так и по значениям")
struct = input()

json_orig = read_json("appsettings.json")

json_new = read_json("new_appsettings.json")

if struct == "struct":
  struct = True
else:
  struct = False
file_comprasion(json_orig, json_new, struct)